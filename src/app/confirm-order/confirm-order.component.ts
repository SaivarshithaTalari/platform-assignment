import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-confirm-order',
  templateUrl: './confirm-order.component.html',
  styleUrls: ['./confirm-order.component.css']
})
export class ConfirmOrderComponent implements OnInit {
  cartItems: any[] = [];
  totalQuantity: number = 0;
  totalPrice: number = 0;
  name: string = '';
  email: string = '';
  deliveryAddress: string = '';

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
    this.cartItems = this.cartService.getCartItems();
    this.totalQuantity = this.cartItems.reduce((total, item) => total + item.quantity, 0);
    this.totalPrice = this.cartItems.reduce((total, item) => total + (item.price * item.quantity), 0);
    this.name = 'varshitha';
    this.email = 'talarisaivarshitha14@gmail.com';
    this.deliveryAddress = 'Hyderabad, Telangana, 501510';
  }

  getEstimatedDeliveryDate(): string {
    return 'April 30, 2024';
  }
}