import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { Router } from '@angular/router';

interface Product{
  name: string;
  image: string;
  price: number;
  quantity: number;
}

interface CartItem extends Product {
  quantity: number;
}
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartItems: CartItem[] = [];
  constructor(private cartService: CartService, private router: Router) { }

  ngOnInit(): void {
    this.cartItems = this.cartService.getCartItems();
  }

  decreaseQuantity(item: CartItem): void {
    item.quantity--;
    if (item.quantity === 0) {
      this.removeFromCart(item);
    }
  }

  increaseQuantity(item: CartItem): void {
    item.quantity = item.quantity ? item.quantity + 1 : 1;
  }

  removeFromCart(item: CartItem): void {
    const index = this.cartItems.findIndex(cartItem => cartItem === item);
    if (index !== -1) {
      this.cartItems.splice(index, 1);
    }
  }

  getTotalQuantity(): number {
    return this.cartItems.reduce((total, item) => total + item.quantity, 0);
  }


  getTotalPrice(): number {
    return this.cartItems.reduce((total, item) => {
      const priceString = item.price.toString();
      const price = parseFloat(priceString.replace('$', ''));
      return total + (item.quantity * price);
    }, 0);
  }
  
 
  getEstimatedDeliveryDate(): string {
    return 'April 30, 2024';
  }

  checkout(): void {
    this.router.navigate(['/confirm-order']);
  }
}