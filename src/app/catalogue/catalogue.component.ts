import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CartService } from '../cart.service';

import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {
  products: any = [];

  constructor(private http: HttpClient, private cartService: CartService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.http.get('assets/products.json').subscribe((data: any) => {
      this.products = data;
    });
  }

  addToCart(product: any): void {
    this.cartService.addToCart(product);
    product.addedSuccessfully = true;
    setTimeout(() => {
      product.addedSuccessfully = false;
    }, 3000);
  }
}